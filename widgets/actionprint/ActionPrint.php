<?php

namespace Empu\Printer\Widgets;

use Backend\Classes\WidgetBase;

class ActionPrint extends WidgetBase
{
    /**
     * @var string A unique alias to identify this widget.
     */
    protected $defaultAlias = 'actionPrint';

    public function render()
    {
        return $this->makePartial('button');
    }

    public function onPrint()
    {
        // $this->addPrinterAssets();
        // $this->addJs('/plugins/empu/printer/assets/js/escPrint.js');
        // $this->addJs('/plugins/empu/printer/assets/js/websocketPrinter.js');

        // $register = $this->formFindModelObject($recordId);
        // $vars = ['invoice' => $register->invoice];
        // $template = new EscInvoiceTemplate();

        // return Whb::make($template, 'INVOICE')->sendJob($vars);
    }
}
