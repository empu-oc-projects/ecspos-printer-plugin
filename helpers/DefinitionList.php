<?php

namespace Empu\Printer\Helpers;

use Mike42\Escpos\Printer;

class DefinitionList
{
    /**
     * @var Printer
     */
    protected $printer;

    public function __construct(Printer $printer)
    {
        $this->printer = $printer;
    }

    public function printList(array $definitions, $keyWidth = 20, ?int $lineWidth = null)
    {
        foreach ($definitions as $value) {
            [$term, $definition] = $value;

            if($lineWidth) {
                $definition = substr($definition, 0, $lineWidth - ($keyWidth + 2));
            }

            $this->printer->text(str_pad($term, $keyWidth, ' ', STR_PAD_RIGHT));
            $this->printer->text(': ');
            $this->printer->text($definition);
            $this->printer->text("\n");
        }
    }
}
