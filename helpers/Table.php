<?php

namespace Empu\Printer\Helpers;

use Closure;
use Mike42\Escpos\Printer;

class Table
{
    /**
     * @var Printer
     */
    protected $printer;

    /**
     * @var array
     */
    protected $config;

    protected $font = Printer::FONT_A;

    protected $index;

    protected $lines;

    public function __construct(Printer $printer, $config)
    {
        $this->printer = $printer;
        $this->config = $config;

        if (array_key_exists('font', $this->config)) {
            $this->font = $this->config['font'];
        }
    }

    public function printTable($rows, bool $reset = true)
    {
        $this->lines = 0;
        $this->printer->selectPrintMode($this->font);

        $this->printTableHead();
        $this->border();

        $this->index = 0;
        foreach ($rows as $row) {
            $this->tableRow($row);
        }

        $this->border(true);

        $reset && $this->reset();
    }

    public function reset()
    {
        $this->printer->selectPrintMode(Printer::FONT_A);
    }

    protected function printTableHead()
    {
        if (array_key_exists('noHeader', $this->config) && $this->config['noHeader']) {
            return;
        }

        $row = collect($this->config['columns'])->map(function ($cell) {
            return $cell['label'];
        })->toArray();

        $this->border(true);
        $this->tableRow($row, true, true);
    }

    public function border(bool $bold = false)
    {
        if (array_key_exists('noBorder', $this->config) && $this->config['noBorder']) {
            return;
        }

        $bold && $this->printer->selectPrintMode($this->font | Printer::MODE_EMPHASIZED);
        $this->printer->text(str_repeat('-', $this->config['width']) . "\n");
        $bold && $this->printer->selectPrintMode($this->font);

        $this->lines++;
    }

    public function tableRow($row, bool $bold = false, bool $raw = false)
    {
        $rowContinue = collect();

        $bold && $this->printer->selectPrintMode($this->font | Printer::MODE_EMPHASIZED);
        foreach ($this->config['columns'] as $key => $config) {
            $text = $raw ? $row[$key] : $this->getValue($row, $config);
            $rowContinue->push($this->tableCell($text, $config));
        }
        $this->printer->text("\n");
        $bold && $this->printer->selectPrintMode($this->font);

        $allDone = $rowContinue->every(function ($remainText) {
            return is_null($remainText);
        });

        if ($allDone) {
            $this->index++;
        } else {
            $this->tableRow($rowContinue->toArray(), $bold, true);
        }

        $this->lines++;
    }

    protected function getValue($row, array $config): ?string
    {
        $value = $config['value'];

        if ($value instanceof Closure) {
            return $value($row, $this->index);
        }
        elseif(is_array($row) && array_key_exists($value, $row)) {
            return $row[$value];
        }
        elseif (is_object($row)) {
            return $row->{$value};
        }
        else {
            return null;
        }
    }

    public function tableCell(?string $text, array $config): ?string
    {
        $space = $config['width'] - 2;
        $content = substr($text, 0, $space);
        $padding = $config['padding'] ?? STR_PAD_RIGHT;

        $this->printer->text(str_pad(" $content ", $config['width'], ' ', $padding));

        return $content == $text ? null : substr($text, $space);
    }

    public function countLines(): int
    {
        return $this->lines;
    }
}
