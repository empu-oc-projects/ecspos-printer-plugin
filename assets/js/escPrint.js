var printerStatus = false;
var printService = $(document).websocketPrinter({
  onConnect: function () {
    printerStatus = true;
  },
  onDisconnect: function () {
    printerStatus = false;
  },
});

function printData(data) {
  if (data.whbData === undefined) {
    return alert(data.errorMessage || 'Kesalahan perintah cetak!');
  }

  if (printerStatus) {
    printService.submit(data.whbData);
  }
  else {
    alert('Printer belum terhubung!');
  }
}