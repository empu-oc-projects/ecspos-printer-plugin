<?php

namespace Empu\Printer\Contracts;

use System\Models\File;

interface ArchivingContent
{
    /**
     * Archiving content string
     *
     * @param string $content
     * @return string file path
     */
    public function archive(string $content): File;

    public function getArchive(): ?File;

    public function archiveName(): string;
}