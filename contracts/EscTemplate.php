<?php

namespace Empu\Printer\Contracts;

use Mike42\Escpos\Printer;

interface EscTemplate
{
    /**
     * Create string buffer
     *
     * @param \Mike42\Escpos\Printer $printer
     * @param array $vars
     * @return \Mike42\Escpos\Printer
     */
    public function draft(Printer $printer, array $vars = []): Printer;
}